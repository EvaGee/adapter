package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements  
        AdapterView.OnItemSelectedListener {  
    String[] courses = { "BSc. Computer Science", 
    "BSc. Applied Computer Science", 
    "BSc. IT", 
    "Diploma in Computer Science", 
    "MSc. Computer Science"};  
  
    @Override  
    protected void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.activity_main);  
       //Getting the instance of Spinner and applying OnItemSelectedListener on it  
        Spinner spin = (Spinner) findViewById(R.id.spinner);  
        spin.setOnItemSelectedListener(this);  
  
        //Creating the ArrayAdapter instance having the courses list  
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,courses);  
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
        //Setting the ArrayAdapter data on the Spinner  
        spin.setAdapter(aa);  
  
    }  
  
    //Performing action onItemSelected and onNothing selected  
    @Override  
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {  
        Toast.makeText(getApplicationContext(),courses[position] , Toast.LENGTH_LONG).show();  
    }  
    @Override  
    public void onNothingSelected(AdapterView<?> arg0) {  
        // TODO Auto-generated method stub  
    }  
